/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-05-15     RT-Thread    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <fal.h>

#include <dfs_fs.h>
#include <dfs_romfs.h>
#include <dfs_ramfs.h>
#include <dfs_posix.h>

#define DBG_TAG "main"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

/* defined the LED0 pin: PH10 */
#define LED0_PIN    GET_PIN(H, 10)

int main(void)
{
    int count = 1;
    rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);

    while (count++)
    {
        rt_pin_write(LED0_PIN, PIN_HIGH);
        rt_thread_mdelay(1000);
        rt_pin_write(LED0_PIN, PIN_LOW);
        rt_thread_mdelay(1000);
    }

    return RT_EOK;
}
rt_uint8_t rampool[128];

void sd_mount(void *parameter)
{
    while (1)
    {
        rt_thread_mdelay(500);
        if(rt_device_find("sd0") != RT_NULL)
        {
            if (dfs_mount("sd0", "/fatfs", "elm", 0, 0) == RT_EOK)
            {
                LOG_I("sd card mount to '/fatfs'");
                break;
            }
            else
            {
                LOG_W("sd card mount to '/fatfs' failed!");
            }
        }
    }
}

int fs_init(void)
{
    /* partition initialized */
    fal_init();

    if(dfs_mount (RT_NULL,"/","rom",0,&(romfs_root)) == 0)
    {
        LOG_I("ROM file system initializated;\n");
    }
    else
    {
        LOG_I("ROM file system initializate failed;\n");
    }

    if(dfs_mount (RT_NULL,"/ram","ram",0,dfs_ramfs_create(rampool, sizeof(rampool))) == 0)
    {
        LOG_I("ram file system initializated;\n");
    }
    else
    {
        LOG_I("ram file system initializate failed;\n");
    }

    /* Create a block device on the file system partition of spi flash */
    struct rt_device *flash_dev = fal_mtd_nor_device_create("filesystem");

    if (flash_dev == RT_NULL)
    {
        LOG_I("Can't create a mtd device on '%s' partition.", "filesystem");
    }
    else
    {
        LOG_I("Create a mtd device on the %s partition of flash successful.", "filesystem");
    }
    /* mount the file system from "filesystem" partition of spi flash. */
    if (dfs_mount(flash_dev->parent.name, "/littlefs", "lfs", 0, 0) == 0)
    {
        LOG_I("littlefs initialized!");
    }
    else
    {
        dfs_mkfs("lfs", flash_dev->parent.name);
        if (dfs_mount(flash_dev->parent.name, "/", "lfs", 0, 0) == 0)
        {
            LOG_I("littlefs initialized!");
        }
    }

    rt_thread_t tid;

    tid = rt_thread_create("sd_mount", sd_mount, RT_NULL,
                           1024, RT_THREAD_PRIORITY_MAX - 2, 20);
    if (tid != RT_NULL)
    {
        rt_thread_startup(tid);
    }
    else
    {
        LOG_E("create sd_mount thread err!");
    }
    return 0;
}
INIT_COMPONENT_EXPORT(fs_init);

#define WIFI_RESET_PIN                 GET_PIN(G,  9)
int ewm1062_disable(void)
{
    rt_pin_mode(WIFI_RESET_PIN, PIN_MODE_OUTPUT);
    rt_pin_write(WIFI_RESET_PIN,PIN_LOW);
    return RT_EOK;
}
INIT_BOARD_EXPORT(ewm1062_disable);

/**
 * Function    ota_app_vtor_reconfig
 * Description Set Vector Table base location to the start addr of app(RT_APP_PART_ADDR).
*/
static int ota_app_vtor_reconfig(void)
{
    #define NVIC_VTOR_MASK   0x3FFFFF80
    /* Set the Vector Table base location by user application firmware definition */
    SCB->VTOR = 0x8020000 & NVIC_VTOR_MASK;

    return 0;
}
INIT_BOARD_EXPORT(ota_app_vtor_reconfig);


